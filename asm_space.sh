#!/bin/bash
#
# Shows space on ASM storage
# Tomasz Slawek 18 march 2006
#

ORACLE_SID=`ps -ef|grep asm_smon| awk -F_ '{print $3}'| grep -v '^\s*$'`


echo Seting Database Enviroment to $ORACLE_SID
export ORAENV_ASK=NO

. oraenv

export ORACLE_BASE ORACLE_HOME ORACLE_SID ORACLE_TERM ORAENV_ASK ORAHOME ORASID
export ORAENV_ASK=YES


${ORACLE_HOME}/bin/sqlplus -s /nolog <<EOF
set time off
set timing off
set feed off
set pages 10000
set lin 200

connect / as sysdba

----------------------------------------SQL-----------------------------------
select substr(NAME,0,18) as "DISKGROUP",trunc(TOTAL_MB/1024) TOTAL_GB ,trunc(FREE_MB/1024) FREE_GB,trunc((TOTAL_MB-FREE_MB)/1024) AS USED_GB,trunc(free_mb/decode(total_mb,0,1,total_mb)*100) FREE_PROC,state from v\$asm_diskgroup order by 1;

----------------------------------------SQL-----------------------------------
EOF

exit
