--
-- Copyright (c) 1988, 2005, Oracle.  All Rights Reserved.
--
-- NAME
--   glogin.sql
--
-- DESCRIPTION
--   SQL*Plus global login "site profile" file
--
--   Add any SQL*Plus commands here that are to be executed when a
--   user starts SQL*Plus, or uses the SQL*Plus CONNECT command.
--
-- USAGE
--   This script is automatically run
--


--Tomek Improvments
SET SQLPROMPT '&_USER.@&_CONNECT_IDENTIFIER.> '
set lin 200
set pages 2000
DEFINE _EDITOR         = "vi"
